# simpsyn

A simple synthesizer written in Python


## Usage

```
$ simpsyn.py <abc-tune>
```

, where <abc-tune> is a sequence of notes following the
[abc standard v2.1](http://abcnotation.com/wiki/abc:standard:v2.1)

### Example

```
$ simpsyn.py cdeccdecdef2def2
```

## Installation and requirements

We recommend to clone the git repository and set up a virtual environment.
Make sure to have pip installed.

```
$ git clone git@gitlab.com:szs/simpsyn.git
$ cd simpsyn
$ python3 -m venv env-simpsyn
$ source env-simpsyn/bin/activate
$ pip install -r requirements.txt
```

## Author

Steffen Brinkmann <s-b@mailbox.org>

## Copyright

© 2018, Steffen Brinkmann, published under the terms of the [MIT license](https://opensource.org/licenses/MIT)
